
# for vm i am using virtualbox and vagrant
[Virtual Box](https://www.virtualbox.org/wiki/Downloads) & [Vagrant](https://www.vagrantup.com/)

# master node and worker node installation on centos

# install docker Engine 
[install Docker ](https://docs.docker.com/engine/install/centos/) 
```
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

```
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```
```
sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
```
sudo systemctl start docker
sudo systemctl enable docker

```
# Remove the config.toml file from /etc/containerd/ Folder and run reload your system daemon
```
rm -f /etc/containerd/config.toml
systemctl daemon-reload
systemctl restart containerd
```
# Disable Swap
```
swapoff -a
```
```
echo '1' > /proc/sys/net/ipv4/ip_forward
modprobe bridge
modprobe br_netfilter
```
[install kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

# Set SELinux in permissive mode (effectively disabling it)
```
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```
# This overwrites any existing configuration in /etc/yum.repos.d/kubernetes.repo
```
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://pkgs.k8s.io/core:/stable:/v1.30/rpm/
enabled=1
gpgcheck=1
gpgkey=https://pkgs.k8s.io/core:/stable:/v1.30/rpm/repodata/repomd.xml.key
exclude=kubelet kubeadm kubectl cri-tools kubernetes-cni
EOF
```
# Install kubelet, kubeadm and kubectl
```
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

```
```
sudo systemctl enable --now kubelet
```
# finaly install kubeadm 
```
kubeadm init --pod-network-cidr=172.0.0.0/16
```
# if geeting init error then 
```
kubeadm init --pod-network-cidr=172.0.0.0/16 --ignore-preflight-errors=all

```
# start using your cluster, you need to run the following as a regular user
```
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
# Alternatively, if you are the root user, you can run:
```
export KUBECONFIG=/etc/kubernetes/admin.conf
```
# To regenrate the tokens
```
kubeadm token create --print-join-command
```
[install calico network ](https://docs.tigera.io/calico/latest/getting-started/kubernetes/quickstart) 

```
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/tigera-operator.yaml
```
```
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.0/manifests/custom-resources.yaml
```
# Confirm that all of the pods are running with the following command
```
watch kubectl get pods -n calico-system
```
# Confirm that you now have a node in your cluster with the following command
```
kubectl get nodes -o wide
```


